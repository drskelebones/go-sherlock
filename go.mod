module main

go 1.16

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/logrusorgru/aurora v2.0.3+incompatible
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
