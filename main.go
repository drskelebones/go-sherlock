package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/dustin/go-humanize"
	. "github.com/logrusorgru/aurora"
	"gopkg.in/yaml.v3"
)

type Site struct {
	Name          string `yaml:"name"`
	URL           string `yaml:"url"`
	ExistsString  string `yaml:"account_existence_string"`
	MissingString string `yaml:"account_missing_string"`
}

type SiteList struct {
	Sites []Site `yaml:"sites"`
}

var (
	plus      = fmt.Sprintf("%s", Green("+"))
	minus     = fmt.Sprintf("%s", Red("-"))
	exclaim   = fmt.Sprintf("%s", Red("!"))
	notfound  = fmt.Sprintf("%s", Yellow("URL is valid but page does not exist."))
	bodyerror = fmt.Sprintf("%s", Yellow("Error found within site body."))
	verbose   = false
)

func Search(wg *sync.WaitGroup, site Site, username string) {
	url := strings.ReplaceAll(site.URL, "%", username)
	defer wg.Done()

	tr := &http.Transport{
		MaxIdleConns:       10,
		IdleConnTimeout:    30 * time.Second,
		DisableCompression: true,
	}

	client := http.Client{
		Timeout:   time.Second * 10,
		Transport: tr,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	response, err := client.Get(url)
	if err != nil {
		return
	}
	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return
	}

	body_error := strings.Contains(string(body), site.MissingString)

	if response.StatusCode < 300 && response.StatusCode >= 200 && !body_error {
		fmt.Printf("[%v] %s: %s\n", plus, Magenta(site.Name), url)
	} else if verbose == true {
		if body_error {
			fmt.Printf("[%v] %s: %s\n", exclaim, Magenta(site.Name), bodyerror)
		} else {
			fmt.Printf("[%v] %s: %s\n", minus, Magenta(site.Name), notfound)
		}
	}
}

func ParseArgs(args []string) []string {
	args[0] = args[len(args)-1]
	args[len(args)-1] = ""
	args = args[:len(args)-1]

	for i := 0; i < len(args); i++ {
		if args[i] == "-v" || args[i] == "--verbose" {
			verbose = true
			args[i] = args[len(args)-1]
			args[len(args)-1] = ""
			args = args[:len(args)-1]
		}
	}
	return args
}

func main() {
	var wg sync.WaitGroup
	args := os.Args
	args = ParseArgs(args)

	start_time := time.Now()
	fmt.Println(Magenta("Beginning search."))

	s, err := ioutil.ReadFile("sites.yml")
	if err != nil {
		panic(err)
	}

	var sites SiteList
	err = yaml.Unmarshal(s, &sites)
	if err != nil {
		panic(err)
	}
	for _, u := range args {
		fmt.Printf("%v %s\n", Magenta("Results for:"), u)
		for _, site := range sites.Sites {
			wg.Add(1)
			go Search(&wg, site, u)
		}
		wg.Wait()
	}

	wg.Wait()
	fmt.Printf("Started %s\n", humanize.Time(start_time))
}
