<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/egonelbre/gophers/raw/master/.thumb/animation/gopher-dance-long-3x.gif">
    <img src="https://github.com/egonelbre/gophers/raw/master/.thumb/animation/gopher-dance-long-3x.gif" alt="Logo">
  </a>

  <h3 align="center">go-sherlock</h3>

  <p align="center">
    go-sherlock is an implementation of [sherlock](https://github.com/sherlock-project/sherlock) written in Go with 
    some changes for better performance and more accurate results.
	<br />
    Provided by Skele's Choice.
	<br />
    Skele's Choice, it's the best choice! Ask your bones!
    <br />
    <br />
    <a href="https://skele.company">skele.company</a>
  </p>
</div>


## Inspiration, Reference, & Credits

- [sherlock](https://github.com/sherlock-project/sherlock)
- [WhatsMyName](https://github.com/WebBreacher/WhatsMyName)
- [Effective Go](https://go.dev/doc/effective_go)
- [Gophers](https://github.com/egonelbre/gophers)

## Libraries
- [go-humanize](https://github.com/dustin/go-humanize)
- [Aurora](https://github.com/logrusorgru/aurora)

## Usage

![demo](https://cdn.skele.dev/fresh-untimely-gullible-every-mountaincat/direct)

Similar to sherlock, go-sherlock can be provided a single or multiple usernames. The usernames will be looked for online and printed out. 

### Option Flags

- -v | --verbose - verbose output, prints URLs which exist but failed to load. 
